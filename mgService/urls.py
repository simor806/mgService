"""mgService URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from app import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    #url(r'^admin/', admin.site.urls),
    #url(r'^edit/(?P<zmiennaklucza>\d+)$', 'app.views.edit')
    #url(r'^new/', 'app.views.new')
    #url(r'', 'app.views.home', name = 'home'),

    url(r'^admin/', admin.site.urls),
    url(r'^pdfRepair/(?P<pkRepair>\d+)$', views.pdfRepair, name='pdfRepair'),
    url(r'^login/$', auth_views.login, {'template_name':'login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page':'home'}, name='logout'),
    url(r'^newClient/', views.newClient, name='newClient'),
    url(r'^newVehicle/', views.newVehicle, name='newVehicle'),
    url(r'^newRepair/(?P<pkVehicle>\d+)$', views.newRepair, name='newRepair'),
    url(r'^newDefaultRepair/', views.newDefaultRepair, name='newDefaultRepair'),
    url(r'^editClient/(?P<pkClient>\d+)$', views.editClient, name='editClient'),
    url(r'^editVehicle/(?P<pkVehicle>\d+)$', views.editVehicle, name='editVehicle'),
    url(r'^editRepair/(?P<pkRepair>\d+)$', views.editRepair, name='editRepair'),
    url(r'^editDefaultRepair/(?P<pkDefaultRepair>\d+)$', views.editDefaultRepair, name='editDefaultRepair'),
    url(r'^delClient/(?P<pkClient>\d+)$', views.deleteClient, name='delClient'),
    url(r'^delVehicle/(?P<pkVehicle>\d+)$', views.deleteVehicle, name='delVehicle'),
    url(r'^delDefaultRepair/(?P<pkDefaultRepair>\d+)$', views.deleteDefaultRepair, name='delDefaultRepair'),
    url(r'^delRepair/(?P<pkRepair>\d+)$', views.deleteRepair, name='delRepair'),
    url(r'^manageClient/', views.manageClient, name='manageClient'),
    url(r'^manageDefaultRepair/(?P<pkDefaultRepair>\d+)$', views.manageDefaultRepair, name='manageDefaultRepair'),
    url(r'^manageDefaultRepair/', views.manageDefaultRepair, name='manageDefaultRepair'),
    url(r'^infoVehicle/(?P<pkVehicle>\d+)$', views.infoVehicle, name='infoVehicle'),
    url(r'', views.home, name='home'),

]
