from django import forms
from app.models import *
from django.forms import ModelForm, Textarea, CheckboxSelectMultiple, HiddenInput
from django.contrib.admin.widgets import *
from datetime import datetime

class ClientForm(ModelForm):
    name = forms.CharField(label="Imię", max_length=50,  required=False)
    surname = forms.CharField(label="Nazwisko", max_length=50)
    contact = forms.CharField(label="Kontakt", max_length=30, required=False)
    comment = forms.CharField(label="Uwagi", max_length=3000, required=False, widget=forms.Textarea(attrs={'rows': 3}))
    class Meta:
        model = Client
        fields = '__all__'

class VehicleForm(ModelForm):
    make = forms.CharField(label="Marka", max_length=20)
    model = forms.CharField(label="Model", max_length=20)
    engine = forms.CharField(label="Silnik", max_length=20, required=False)
    productionYear = forms.CharField(label="Rok produkcji", max_length=4, required=False)
    registrationNumber = forms.CharField(label="Numer rejestracyjny", max_length=10)
    vinNumber = forms.CharField(label="Numer VIN", max_length=17, required=False)
    owners = forms.ModelMultipleChoiceField(label="Właściciel", queryset=Client.objects.all())
    class Meta:
        model = Vehicle
        fields = '__all__'
        widgets = {
            'owners': FilteredSelectMultiple('klienci', False)
        }
    def clean_registrationNumber(self):
        return self.cleaned_data["registrationNumber"].upper()
    def clean_vinNumber(self):
        return self.cleaned_data["vinNumber"].upper()

class RepairForm(ModelForm):
    date = forms.DateField(label="Data")
    mileage = forms.IntegerField(label="Przebieg")
    comment = forms.CharField(label="Uwagi", max_length=3000, required=False, widget=forms.Textarea(attrs={'rows': 6}))
    doneDefaultRepairs = forms.ModelMultipleChoiceField(label="Wykonane naprawy", queryset=DefaultRepair.objects.all(), widget=forms.SelectMultiple())
    doneOtherRepairs = forms.CharField(label='Inne naprawy', max_length=1000, required=False, widget=forms.Textarea(attrs={'rows': 4}))
    #image = forms.ImageField(label="Zdjęcie", required=False, widget=forms.ClearableFileInput())
    vehicle = forms.ModelChoiceField(label="Pojazd", queryset=Vehicle.objects.all(), widget=forms.HiddenInput())
    class Meta:
        model = Repair
        #fields = '__all__'
        exclude = ('image',)
    def clean_date(self):
        repairDate = self.cleaned_data["date"]
        now = datetime.now().date()
        if now < repairDate:
            raise forms.ValidationError("Data naprawy musi być mniejsza lub równa dzisiejszej")
        return repairDate

class DefaultRepairForm(ModelForm):
    name = forms.CharField(label="Nazwa")
    class Meta:
        model = DefaultRepair
        fields = '__all__'
