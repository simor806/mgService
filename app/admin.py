from django.contrib import admin
from app.models import *

# Register your models here.

admin.site.register(Client)
admin.site.register(Vehicle)
admin.site.register(Repair)
admin.site.register(DefaultRepair)
