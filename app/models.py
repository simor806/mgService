from django.db import models
from datetime import datetime

class Client(models.Model):
    name = models.CharField('Imię', max_length = 50, null  = True, blank = True)
    surname = models.CharField('Nazwisko', max_length = 50)
    contact = models.CharField('Kontakt', max_length = 30, blank = True)
    comment = models.TextField('Notatki', null = True, blank = True)
    @property
    def getVehicles(self):
        vehicles = Vehicle.objects.filter(owners=self.id)
        return vehicles
    def __str__ (self):
        return self.surname + ' ' + self.name

class Vehicle(models.Model):
    make = models.CharField('Marka', max_length = 20)
    model = models.CharField('Model', max_length = 20)
    engine = models.CharField('Silnik', max_length = 20, null = True, blank = True)
    productionYear = models.CharField('Rok produkcji', max_length = 4, null = True, blank = True)
    registrationNumber = models.CharField('Numer rejestracyjny', max_length = 10)
    vinNumber = models.CharField('Numer VIN', max_length = 17, null = True, blank = True)
    owners = models.ManyToManyField(Client, related_name = "Owners")
    @property
    def getFullName(self):
        return self.make + ' ' + self.model + ' ' + self.engine + ' (' + self.productionYear + ') '
    @property
    def getOwners(self):
        return "; ".join([str(owner) for owner in self.owners.all()])
    @property
    def getMileage(self):
        lastRepair = Repair.objects.filter(vehicle=self.id).latest('date')
        return lastRepair.mileage
    @property
    def checkOilByDate(self):
        try:
            lastRepairWitchOilChange = Repair.objects.filter(vehicle=self.id, doneDefaultRepairs__name="Wymiana oleju").latest('date')
            now = datetime.now().date()
            dateDifference = (now - lastRepairWitchOilChange.date).days
            if dateDifference < 365:
                return None
            else:
                return lastRepairWitchOilChange.date
        except Repair.DoesNotExist:
            return None
    @property
    def checkOilByMileage(self):
        try:
            lastRepairWitchOilChange = Repair.objects.filter(vehicle=self.id, doneDefaultRepairs__name = "Wymiana oleju").latest('date')
            currentMileage = self.getMileage
            mileageDifference = currentMileage - lastRepairWitchOilChange.mileage
            if mileageDifference < 15000:
                return None
            else:
                return mileageDifference
        except Repair.DoesNotExist:
            return None
    def __str__ (self):
        return self.make + ' ' + self.model + ' (' + self.registrationNumber + ')'

class DefaultRepair(models.Model):
    name = models.CharField('Rodzaj usługi', max_length = 100)
    class Meta:
        ordering = ['-name']
    def __str__(self):
        return self.name

class Repair(models.Model):
    date = models.DateField('Data naprawy', db_index = True)
    mileage = models.PositiveIntegerField('Przebieg')
    doneDefaultRepairs = models.ManyToManyField(DefaultRepair, related_name = "DefaultRepairs")
    doneOtherRepairs = models.CharField('Inne naprawy', max_length = 1000, null = True, blank = True)
    comment = models.TextField('Notatki', null = True, blank = True)
    image = models.ImageField('Zdjęcia', upload_to = 'user_files/photos', null = True, blank = True)
    vehicle = models.ForeignKey(Vehicle)
    class Meta:
        ordering = ['-date']
    def __str__ (self):
        return str(self.date)
