from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from app.models import *
from app.forms import *
from reportlab.pdfgen import canvas
from django.contrib.auth.decorators import login_required

def home(request):
    allVehicles = Vehicle.objects.all().order_by("-registrationNumber")
    return render(request, "app/index.html", {'allVehicles': allVehicles})

@login_required
def newClient(request):
    allClients = Client.objects.all()
    if request.method == 'POST':
        form = ClientForm(data = request.POST)
        if form.is_valid():
            try:
                newClient = Client()
                newClient.name = form.cleaned_data['name']
                newClient.surname = form.cleaned_data['surname']
                newClient.contact = form.cleaned_data['contact']
                newClient.comment = form.cleaned_data['comment']
                newClient.save()
                return redirect('manageClient')
            except:
                return Http404('Błąd podczas zapisu danych!')
        else:
            return render(request, "app/client_manage.html", {'allClients': allClients, 'form': form, 'showForm': True})
    else:
        form = ClientForm()
    return render(request, "app/client_manage.html", {'allClients': allClients, 'form': form, 'showForm': True})

@login_required
def newVehicle(request):
    allVehicles = Vehicle.objects.all()
    if request.method == 'POST':
        form = VehicleForm(data = request.POST)
        if form.is_valid():
            try:
                newVehicle = form.save(commit = False)
                newVehicle.save()
                form.save_m2m()
                return redirect('home')
            except:
                return Http404('Błąd podczas zapisu danych!')
        else:
            return render(request, "app/index.html", {'allVehicles': allVehicles, 'form': form, 'showForm': True})
    else:
        form = VehicleForm()
    return render(request, "app/index.html", {'allVehicles': allVehicles, 'form': form, 'showForm': True})

@login_required
def newRepair(request, pkVehicle):
    if request.method == 'POST':
        form = RepairForm(data = request.POST)
        if form.is_valid():
            try:
                newRepair = form.save(commit = False)
                newRepair.save()
                form.save_m2m()
            except:
                raise Http404('Błąd podczas zapisu danych!')
        else:
            vehicle = Vehicle.objects.get(pk = pkVehicle)
            return render(request, "app/vehicle_info.html", {'vehicle' : vehicle, 'form': form, 'showForm': True})
    return redirect('infoVehicle', pkVehicle)

@login_required
def newDefaultRepair(request):
    if request.method == 'POST':
        form = DefaultRepairForm(data = request.POST)
        if form.is_valid():
            try:
                newDefaultRepair = form.save()
                newDefaultRepair.save()
            except:
                raise Http404('Błąd podczas zapisu danych!')
        else:
            allRepairs = DefaultRepair.objects.all()
            return render(request, "app/defaultRepair_manage.html", {'allRepairs': allRepairs, 'form': form, 'showForm': True})
    return redirect('manageDefaultRepair')

def editClient(request, pkClient):
    if request.method == 'POST':
        form = ClientForm(data = request.POST)
        if form.is_valid():
            try:
                client = Client.objects.get(pk = pkClient)
                client.name = form.cleaned_data['name']
                client.surname = form.cleaned_data['surname']
                client.contact = form.cleaned_data['contact']
                client.comment = form.cleaned_data['comment']
                client.save()
                return redirect('manageClient')
            except:
                raise Http404("Klient do edycji nie istnieje!")
    else:
        client = Client.objects.get(pk = pkClient)
        form = ClientForm(instance=client)
    return render(request, "app/client_edit.html", {'client' : client, 'form': form})

def editVehicle(request, pkVehicle):
    try:
        vehicle = Vehicle.objects.get(pk = pkVehicle)
    except Vehicle.DoesNotExist:
        raise Http404("Nie ma takiego pojazdu!")
    if request.method == 'POST':
        form = VehicleForm(request.POST, instance = vehicle)
        if form.is_valid():
            try:
                vehicle = form.save(commit = False)
                vehicle.save()
                form.save_m2m()
                return redirect('infoVehicle', pkVehicle=vehicle.id)
            except:
                raise Http404("Błąd formularza!")
    else:
        vehicle = Vehicle.objects.get(pk = pkVehicle)
        form = VehicleForm(instance=vehicle)
    return render(request, "app/vehicle_edit.html", {'vehicle' : vehicle, 'form': form})

def editRepair(request, pkRepair):
    try:
        repair = Repair.objects.get(pk = pkRepair)
    except Repair.DoesNotExist:
        raise Http404("Nie ma takiego wpisu!")
    if request.method == 'POST':
        form = RepairForm(request.POST, instance = repair)
        if form.is_valid():
            try:
                repair = form.save(commit = False)
                repair.save()
                form.save_m2m()
                return redirect('infoVehicle', pkVehicle=repair.vehicle.id)
            except:
                raise Http404("Błąd formularza!")
    else:
        form = RepairForm(instance=repair)
    return render(request, "app/repair_edit.html", {'repair' : repair, 'form': form})

def editDefaultRepair(request, pkDefaultRepair):
    try:
        repair = DefaultRepair.objects.get(pk = pkDefaultRepair)
    except DefaultRepair.DoesNotExist:
        raise Http404("Nie ma takiej domyślnej naprawy!")
    if request.method == 'POST':
        form = DefaultRepairForm(data = request.POST)
        print(form)
        if form.is_valid():
            try:
                repair.name = form.cleaned_data['name']
                repair.save()
                return redirect('manageDefaultRepair')
            except:
                raise Http404("Błąd formularza!")
    return redirect('home')

def deleteClient(request, pkClient):
	try:
		Client.objects.get(pk = pkClient).delete()
	except Client.DoesNotExist:
		raise Http404("Klient nie istnieje!")
	return redirect('manageClient')

def deleteVehicle(request, pkVehicle):
	try:
		Vehicle.objects.get(pk = pkVehicle).delete()
	except Vehicle.DoesNotExist:
		raise Http404("Pojazd nie istnieje!")
	return redirect('home')

def deleteRepair(request, pkRepair):
	try:
		repair = Repair.objects.get(pk = pkRepair)
		vehicleId = repair.vehicle.id
		Repair.objects.get(pk = pkRepair).delete()
	except Repair.DoesNotExist:
		raise Http404("Błąd! Nie ma takiego wpisu!")
	return redirect('infoVehicle', pkVehicle=vehicleId)

def deleteDefaultRepair(request, pkDefaultRepair):
	try:
		DefaultRepair.objects.get(pk = pkDefaultRepair).delete()
	except DefaultRepair.DoesNotExist:
		raise Http404("Nie ma takiej nazwy standardowej naprawy!")
	return redirect('manageDefaultRepair')

@login_required
def infoVehicle(request, pkVehicle):
    vehicle = Vehicle.objects.get(pk = pkVehicle)
    repairs = Repair.objects.filter(vehicle = pkVehicle).order_by("-date")
    form = RepairForm(initial={'vehicle': vehicle.pk})
    return render(request, "app/vehicle_info.html", {'vehicle' : vehicle, 'repairs': repairs, 'form': form})

@login_required
def manageClient(request, pkClient=None):
    allClients = Client.objects.all()
    if (pkClient != None):
        client = Client.objects.get(pk = pkClient)
        idClient = client.id;
        form = Client(instance=client)
    else:
        idClient = None
        form = ClientForm()
    return render(request, "app/client_manage.html", {'allClients' : allClients, 'idClient': idClient, 'form': form})

@login_required
def manageDefaultRepair(request, pkDefaultRepair=None):
    allRepairs = DefaultRepair.objects.all()
    if (pkDefaultRepair != None):
        repair = DefaultRepair.objects.get(pk = pkDefaultRepair)
        idRepair = repair.id;
        form = DefaultRepairForm(instance=repair)
    else:
        idRepair = None
        form = DefaultRepairForm()
    return render(request, "app/defaultRepair_manage.html", {'allRepairs' : allRepairs, 'idRepair': idRepair, 'form': form})

def pdfRepair(request, pkRepair):
	#maxPageSize(600, 790)
	repair = Repair.objects.get(pk = pkRepair)

	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition'] = 'filename="naprawa_' + str(repair.id) + '.pdf"'

	pdf = canvas.Canvas(response)

	pdf.setFont('Verdana', 17)
	marginLeft = 50
	marginTop = 780

	vehicle = Vehicle.objects.get(pk = repair.vehicle.id)
	pdfVehicle = str(vehicle.registrationNumber) + ' - ' + vehicle.getFullName
	pdf.drawString(marginLeft, marginTop, pdfVehicle)

	marginTop -= 10
	pdf.setLineWidth(0.3);
	pdf.line(marginLeft, marginTop, 600 - marginLeft, marginTop);

	
	marginTop -= 30
	pdf.setFont('Verdana', 13)
	pdfDate = "Data: " + str(repair.date)
	pdf.drawString(marginLeft, marginTop, "Data: ")

	marginTop -= 20
	pdf.setFont('Verdana', 11)
	pdf.drawString(marginLeft, marginTop, str(repair.date))

	marginTop -= 30
	pdf.setFont('Verdana', 13)
	pdf.drawString(marginLeft, 690, "Wykonane naprawy:")
	
	marginTop -= 20
	pdf.setFont('Verdana', 11)
	doneRepairs = Repair.objects.filter(id=pkRepair).values_list('doneDefaultRepairs', flat=True)
	i = 0
	for doneRepair in doneRepairs:
		pdfDoneDefaultRepairs = '- ' + str(DefaultRepair.objects.get(id=doneRepair))
		marginTop -= i
		pdf.drawString(marginLeft, marginTop, str(pdfDoneDefaultRepairs))
		i += 15
		
	if repair.doneOtherRepairs:
		doneOtherRepairs = str(repair.doneOtherRepairs).replace('\r', '').split('\n')
		for doneOtherRepair in doneOtherRepairs:
			marginTop -= 15
			pdf.drawString(marginLeft, marginTop, "- " + doneOtherRepair)

	marginTop -= 30
	pdf.setFont('Verdana', 13)
	pdfDate = "Uwagi: " 
	pdf.drawString(marginLeft, marginTop, "Uwagi:")
	
	marginTop -= 20
	pdf.setFont('Verdana', 11)
	pdf.drawString(marginLeft, marginTop, str(repair.comment))
	

	pdf.showPage()
	pdf.save()
	return response
