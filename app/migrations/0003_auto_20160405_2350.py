# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-04-05 21:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20160405_2349'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vehicle',
            name='repairs',
            field=models.ManyToManyField(related_name='Repairs', to='app.Repair'),
        ),
    ]
