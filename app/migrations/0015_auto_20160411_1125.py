# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-04-11 09:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0014_auto_20160411_1101'),
    ]

    operations = [
        migrations.AlterField(
            model_name='repair',
            name='vehicle',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Vehicle'),
        ),
    ]
